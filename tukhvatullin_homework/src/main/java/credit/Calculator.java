package credit;

import java.util.Scanner;

public class Calculator {
    public static void main (String[] args) {
        boolean result = false;
        Scanner input = new Scanner(System.in);
        System.out.println("Введите Ваше имя: ");
        String name = input.nextLine();
        System.out.println("Введите Ваш возраст: ");
        int age = input.nextInt();
        System.out.println("Введите сумму кредита: ");
        int sum = input.nextInt();
        result = (name.equalsIgnoreCase("Bob") || age < 18 || sum > (age*100)) ? false : true;
        if(result){
            System.out.println("Кредит одобрен!");
        }
        else {
            System.out.println("Попробуйте позже!");
        }
    }
}
